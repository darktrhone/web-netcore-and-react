﻿ 
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using React.AspNet;
using Meziantou.AspNetCore.BundleTagHelpers;

namespace WebNetCore
{
    public class Startup
    {

        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // add gzip compression middleware
            services.AddResponseCompression();
            // add response cache middleware
            services.AddResponseCaching(); 

            // Add React
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddReact();

            // Add framework services.
            services.AddMvc();

            // Add Version to scripts un-bundled
            services.AddBundles(options =>
            {
                options.AppendVersion = true;
            });

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //env.EnvironmentName = EnvironmentName.Production;

            // use gzip compression middleware
            app.UseResponseCompression();
            // use response cache middleware
            app.UseResponseCaching();


            //loggerFactory.AddFile("Logs/myapp-{Date}.txt"); 
            //loggerFactory.AddConsole().AddDebug();


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }


            app.UseReact(config =>
            {
                config
                    .AddScript("~/js/remarkable.min.js")
                    .AddScript("~/js/ReactScript.jsx");
            });

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });

        }
    }

}
