﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebNetCore.Models
{
    public class MenuModel
    {
        public string Title { get; set; }
        public string Link { get; set; }
    }
}
