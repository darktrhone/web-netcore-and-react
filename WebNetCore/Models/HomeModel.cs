﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebNetCore.Models
{
    public class HomeModel : BaseModel
    {
        public List<CommentModel> CommentModel { get; set; }
        public List<MenuModel> MenuModel { get; set; }
        public List<PortfolioModel> PortfolioModel { get; set; }
    }
}
