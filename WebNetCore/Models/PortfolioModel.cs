﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebNetCore.Models
{
    public class PortfolioModel
    {
        public string Image { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
