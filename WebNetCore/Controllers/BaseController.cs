﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebNetCore.Models;
using Microsoft.Extensions.Logging;

namespace WebNetCore.Controllers
{
    public class BaseController<T> : Controller where T : BaseModel, new()
    {
        public T Model;
        public ILogger Logger;

        public BaseController(ILogger logger)
        {
            this.Logger = logger;
            this.Model = new T();
        }


        public virtual IActionResult Index()
        {
            return View(this.Model);
        }

    }

}

