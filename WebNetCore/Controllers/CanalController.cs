﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebNetCore.Models;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace WebNetCore.Controllers
{
    public class CanalController : BaseController<HomeModel>
    {
        private static List<CommentModel> _comments = GetComments("ServerSide");

        private static List<MenuModel> _menu = GetMenu();

         
        public CanalController(ILogger<CanalController> logger) : base(logger)
        {

        }


        [ResponseCache(Duration = 60)]
        public override IActionResult Index()
        {
            this.Model.CommentModel = _comments;

            this.Model.MenuModel = _menu;

            return View(this.Model);

        }


        private static List<CommentModel> GetComments(string suffix)
        {
            var date = DateTime.Now.ToString("HH:mm:ss") + "_" + suffix;
            return new List<CommentModel>
            {
                new CommentModel
                {
                    Id = 1,
                    Author = "Daniel Lo Nigro " + date,
                    Text = "Hello ReactJS.NET World!"
                },
                new CommentModel
                {
                    Id = 2,
                    Author = "Pete Hunt " + date,
                    Text = "This is one comment"
                },
                new CommentModel
                {
                    Id = 3,
                    Author = "Jordan Walke " + date,
                    Text = "This is *another* comment"
                },
            };
        }

        private static List<MenuModel> GetMenu()
        {
            return new List<MenuModel>
            {
                new MenuModel
                {
                    Link = "/",
                    Title = "Home"
                },
                    new MenuModel
                {
                    Link = "/Canal",
                    Title = "Canal"
                },
                  new MenuModel
                {
                    Link = "http://www.google.pt",
                    Title = "Google"
                },
                  new MenuModel
                {
                    Link = "http://www.facebook.com",
                    Title = "Facebook"
                }

            };
        }

        [Route("Comments")]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Comments()
        {
            return Json(_comments);

        }

        [Route("AddComments")]
        [HttpPost]
        public ActionResult AddComments(CommentModel comment)
        {
            comment.Id = _comments.Count + 1;
            comment.Author += " " + DateTime.Now.ToString("HH:mm:ss") + "_ClientSide ";
            _comments.Add(comment);
            return Content("Success :)");
        }

    }
}

