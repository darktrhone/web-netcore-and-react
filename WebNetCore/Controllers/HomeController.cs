﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebNetCore.Models;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace WebNetCore.Controllers
{
    public class HomeController : BaseController<HomeModel>
    {
        private static List<CommentModel> _comments = GetComments("ServerSide");

        private static List<MenuModel> _menu = GetMenu();

        private static List<PortfolioModel> _portfolio = GetPortfolio();

        public HomeController(ILogger<HomeController> logger) : base(logger)
        {
        }

        [ResponseCache(Duration = 60)]
        public override IActionResult Index()
        {
            this.Model.CommentModel = _comments;

            this.Model.MenuModel = _menu;

            this.Model.PortfolioModel = _portfolio;

            return View(this.Model);

        }

        private static List<CommentModel> GetComments(string suffix)
        {
            var date = DateTime.Now.ToString("HH:mm:ss") + "_" + suffix;
            return new List<CommentModel>
            {
                new CommentModel
                {
                    Id = 1,
                    Author = "Daniel Lo Nigro " + date,
                    Text = "Hello ReactJS.NET World!"
                },
                new CommentModel
                {
                    Id = 2,
                    Author = "Pete Hunt " + date,
                    Text = "This is one comment"
                },
                new CommentModel
                {
                    Id = 3,
                    Author = "Jordan Walke " + date,
                    Text = "This is *another* comment"
                },
            };
        }

        private static List<MenuModel> GetMenu()
        {
            return new List<MenuModel>
            {
                new MenuModel
                {
                    Link = "/",
                    Title = "Home"
                },
                    new MenuModel
                {
                    Link = "/Canal",
                    Title = "Canal"
                },
                  new MenuModel
                {
                    Link = "http://www.google.pt",
                    Title = "Google"
                },
                  new MenuModel
                {
                    Link = "http://www.facebook.com",
                    Title = "Facebook"
                }

            };
        }

        private static List<PortfolioModel> GetPortfolio()
        {
            return new List<PortfolioModel>
            {
                new PortfolioModel
                {
                    Link = "https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwj_zdGzg_7TAhWF2xoKHbaeBXQQjxwIAw&url=http%3A%2F%2Fmundoeducacao.bol.uol.com.br%2Fgeografia%2Fconceito-paisagem.htm&psig=AFQjCNEeyj390uzXyRMFYI6Z9HvolsBZ7A&ust=1495354405898304",
                    Image ="http://mundoeducacao.bol.uol.com.br/upload/conteudo/paisagem-natural.jpg",
                    Title = "Conceito de Paisagem",
                    Text= "Exemplo de um tipo de paisagem natural sem a intervenção direta do ser humano"
                },
                new PortfolioModel
                {
                    Link = "https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjR9fuehP7TAhWD6xQKHcZ7DtUQjxwIAw&url=https%3A%2F%2Fwww.pinterest.com%2Fgrammy120%2Fautumn%2F&psig=AFQjCNEeyj390uzXyRMFYI6Z9HvolsBZ7A&ust=1495354405898304",
                    Image ="https://blog.emania.com.br/content/uploads/2015/12/Papel-de-Parede-de-Paisagem.jpg",
                    Title = "Paisagens",
                    Text= "3D murais rústico de óleo para sala parede sofá TV fundo papel de parede paisagem de cobre(China (Mainland))"
                },
                new PortfolioModel
                {
                    Link = "https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiGg9fEhP7TAhVMbhQKHZF2BcwQjxwIAw&url=https%3A%2F%2Fwww.elo7.com.br%2Flista%2Fpainel-de-paisagem&psig=AFQjCNEeyj390uzXyRMFYI6Z9HvolsBZ7A&ust=1495354405898304",
                    Image ="https://img.elo7.com.br/product/original/FFFDFE/painel-paisagem-g-frete-gratis-painel-de-festa-infantil.jpg",
                    Title = "Primavera",
                    Text= "E o meu mundo me encanta. E Como não se encantar? Pois olhando esse encanto, eu não me lembro de prantos e lamentos. Me vem à mente sonhos e desejos."
                },
                  new PortfolioModel
                {
                    Link = "https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjiuNiglP7TAhWJRhQKHcvcAngQjxwIAw&url=http%3A%2F%2Fgarden.id%2Ff%2Ffondos-de-rayas-azules-imagui.html&psig=AFQjCNEFFc6lIc1XmTiXSypJTaNrssVUZA&ust=1495358899414235",
                    Image ="https://blog.emania.com.br/content/uploads/2015/12/277277_Papel-de-Parede-Bela-Paisagem-de-Campo_1680x1050.jpg",
                    Title = "Paisagem",
                    Text= "Fundos de paisagens naturais."
                },
            };
        }

        [Route("Comments")]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Comments()
        {
            return Json(_comments);

        }

        [Route("AddComments")]
        [HttpPost]
        public ActionResult AddComments(CommentModel comment)
        {
            comment.Id = _comments.Count + 1;
            comment.Author += " " + DateTime.Now.ToString("HH:mm:ss") + "_ClientSide ";
            _comments.Add(comment);
            return Content("Success :)");
        }

    }
}

