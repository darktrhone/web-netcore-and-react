"use strict";

var gulp = require("gulp"),
    concat = require("gulp-concat"),
    cssmin = require('gulp-cssmin'),
    gutil = require('gulp-util'),
    uglify = require("gulp-uglify"),
    merge = require("merge-stream"),
    react = require('gulp-react'),
    bundleconfig = require("./bundleconfig.json");


var isProduction = (gutil.env.env === 'Production');



gulp.task("Compile:Minify:Bundle", ["min:js", "min:css"]);

gulp.task('compile:jsx', function () {
    gutil.log("Compiling Scripts.");
   return gulp.src("wwwroot/js/ReactScript.jsx")
        .pipe(react())
        .pipe(concat("ReactScript.js"))
        .pipe(gulp.dest("wwwroot/js"));
});

gulp.task("min:js",["compile:jsx"] , function () {
    if (isProduction) {
        var tasks = getBundles(".js").map(function (bundle) {
            gutil.log("Processing Bundle: " + bundle.outputFileName); 

            return gulp.src(bundle.inputFiles, { base: "." })
                .pipe(concat(bundle.outputFileName))
                .pipe(uglify())
                .on('end', function () { gutil.log('Minified :' + bundle.outputFileName); })
                .pipe(gulp.dest("."));

        });
    }
    return merge(tasks);
});

gulp.task("min:css", function () {
    if (isProduction) {
        var tasks = getBundles(".css").map(function (bundle) {

            gutil.log("Processing Bundle: " + bundle.outputFileName); 

            return gulp.src(bundle.inputFiles, { base: "." })
                .pipe(concat(bundle.outputFileName))
                .pipe(cssmin())
                .on('end', function () { gutil.log('Minified :' + bundle.outputFileName); })
                .pipe(gulp.dest("."));

        });
    }
    return merge(tasks);
});

function getBundles(extension) {
    return bundleconfig.filter(function (bundle) {
        return new RegExp(`(?!.*?.dev)^.*${extension}$`).test(bundle.outputFileName);
    });
}
function getUnminifiedFiles() {
    return bundleconfig.filter(function (bundle) {
        return new RegExp(`(?!.*?.min)^.*$`).test(bundle.outputFileName);
    });
} 